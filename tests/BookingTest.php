<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;

class BookingTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	/** @test */
	public function createBooking() {
		$bookingData = array(
			"price" => 100,
			"checkindate" => "2024-11-19 15:00:00",
			"checkooutdate" => "2024-11-20 15:00:00",
			"username" => "testdata",
			"name" => "Eduard",
			"email" => "eduardvives74@gmail.com",
			"phone" => "918955805"
		);

		$results = $this->booking->createBooking($bookingData);
		$this->assertEquals($results['id'], 3);
		$this->assertEquals($results['clientid'], 6);
		$this->assertEquals($results['price'], 100);
		$this->assertEquals($results['checkindate'], "2024-11-19 15:00:00");
		$this->assertEquals($results['checkooutdate'], "2024-11-20 15:00:00");
	}
}