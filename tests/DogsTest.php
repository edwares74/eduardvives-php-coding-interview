<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Dog;

class DogsTest extends TestCase {

	private $dog;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->dog = new Dog();
	}

	/** @test */
	public function getClientDogs() {
		$results = $this->dog->getClientDogs(1);

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
		$this->assertEquals(count($results), 2);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['name'], 'bobby');
		$this->assertEquals($results[0]['age'], 5);

	}
}