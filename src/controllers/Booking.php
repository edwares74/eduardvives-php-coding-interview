<?php

namespace Src\controllers;

use Src\models\BookingModel;
use Src\models\ClientModel;
use Src\models\DogModel;

class Booking
{

	private function getBookingModel(): BookingModel
	{
		return new BookingModel();
	}

	public function getBookings()
	{
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($bookingData)
	{
		$client = new Client();
	
		$clientData = array(
			"username" => $bookingData["username"],
			"name" => $bookingData["name"],
			"email" => $bookingData["email"],
			"phone" => $bookingData["phone"],
		);

		if (isset($bookingData["clientid"])) {

			$clientData = $client->getClientById($bookingData['clientid']) == null ? $client->getClientById($bookingData["clientid"]) : $client->createClient($clientData);
			$bookingData["clientid"] = $clientData['id'];
			
			$hasDiscount = $this->hasDiscount($bookingData["clientid"]);
			if ($hasDiscount) {
				$bookingData['price'] = $bookingData['price']  - ($bookingData['price'] * 0.10);
			}

		} else {

			$createClient = $client->createClient($clientData);
			$bookingData["clientid"] = $createClient["id"];

			$hasDiscount = $this->hasDiscount($bookingData["clientid"]);
			if ($hasDiscount) {
				$bookingData['price'] = $bookingData['price']  - ($bookingData['price'] * 0.10);
			}
			
		}

		return $this->getBookingModel()->createBooking($bookingData);
	}

	public function hasDiscount($id) {
		$dogs = new Dog();

		$clientDogs   = $dogs->getClientDogs($id);
		foreach ($clientDogs as $dog) {
				if ($dog['age'] < 10) {
					return true;
				}
		}

		return false;
	}
}
